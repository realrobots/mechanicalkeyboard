EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Battery BT?
U 1 1 60B86DF4
P 2100 3850
F 0 "BT?" H 2208 3896 50  0000 L CNN
F 1 "2S 7.4v LiPo" H 2208 3805 50  0000 L CNN
F 2 "" V 2100 3910 50  0001 C CNN
F 3 "~" V 2100 3910 50  0001 C CNN
	1    2100 3850
	1    0    0    -1  
$EndComp
$Comp
L LaptopCircuit:TP5100 U?
U 1 1 60B891FF
P 4150 3700
F 0 "U?" V 4079 3938 50  0000 L CNN
F 1 "12v Step Up" V 4170 3938 50  0000 L CNN
F 2 "" H 4150 3700 50  0001 C CNN
F 3 "" H 4150 3700 50  0001 C CNN
	1    4150 3700
	0    1    1    0   
$EndComp
$Comp
L LaptopCircuit:TP5100 U?
U 1 1 60B8B921
P 5600 5000
F 0 "U?" H 5575 5375 50  0000 C CNN
F 1 "5vReg" H 5575 5284 50  0000 C CNN
F 2 "" H 5600 5000 50  0001 C CNN
F 3 "" H 5600 5000 50  0001 C CNN
	1    5600 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 3550 2100 3650
$Comp
L Connector:USB_A J?
U 1 1 60B8F8B1
P 2700 1550
F 0 "J?" V 2711 1880 50  0000 L CNN
F 1 "USB Charging Port" V 2802 1880 50  0000 L CNN
F 2 "" H 2850 1500 50  0001 C CNN
F 3 " ~" H 2850 1500 50  0001 C CNN
	1    2700 1550
	0    1    1    0   
$EndComp
Wire Wire Line
	4300 4050 4300 4200
Wire Wire Line
	4300 4200 4850 4200
Wire Wire Line
	4850 4200 4850 4850
Wire Wire Line
	4850 4850 5200 4850
Connection ~ 4850 4200
Wire Wire Line
	4850 4200 5150 4200
Wire Wire Line
	4000 4050 4000 4450
Wire Wire Line
	4000 5150 5200 5150
Wire Wire Line
	5950 4850 6150 4850
Wire Wire Line
	2900 1850 2900 1950
Wire Wire Line
	2300 1550 2300 2100
Wire Wire Line
	2300 2100 2600 2100
Connection ~ 2100 4050
Wire Wire Line
	2900 2850 2900 2900
Wire Wire Line
	2100 3550 3550 3550
$Comp
L LaptopCircuit:TP5100 U?
U 1 1 60BA35EA
P 2750 2500
F 0 "U?" V 2679 2738 50  0000 L CNN
F 1 "12v Step Up" V 2770 2738 50  0000 L CNN
F 2 "" H 2750 2500 50  0001 C CNN
F 3 "" H 2750 2500 50  0001 C CNN
	1    2750 2500
	0    1    1    0   
$EndComp
$Comp
L LaptopCircuit:TP5100 U?
U 1 1 60BB4538
P 2450 3250
F 0 "U?" H 2425 2885 50  0000 C CNN
F 1 "TP5100" H 2425 2976 50  0000 C CNN
F 2 "" H 2450 3250 50  0001 C CNN
F 3 "" H 2450 3250 50  0001 C CNN
	1    2450 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	3350 3400 3350 2900
Wire Wire Line
	3350 2900 2900 2900
Wire Wire Line
	2100 3400 2100 3550
Connection ~ 2100 3550
Wire Wire Line
	2100 4050 3700 4050
Wire Wire Line
	3700 3300 4000 3300
Wire Wire Line
	3700 3300 3700 4050
Text HLabel 6150 4850 2    50   Input ~ 0
PI+5v
Text HLabel 6150 5150 2    50   Input ~ 0
PI_GND
Wire Wire Line
	5950 5150 6150 5150
Text HLabel 5150 4200 2    50   Input ~ 0
LCD+12v
Text HLabel 5150 4450 2    50   Input ~ 0
LCD_GND
Wire Wire Line
	5150 4450 4000 4450
Connection ~ 4000 4450
Wire Wire Line
	4000 4450 4000 5150
Wire Wire Line
	2850 3400 3350 3400
Wire Wire Line
	2600 2850 2850 2850
Wire Wire Line
	2850 2850 2850 3100
Wire Wire Line
	2100 3100 1850 3100
Wire Wire Line
	1850 3100 1850 4050
Wire Wire Line
	1850 4050 2100 4050
Wire Wire Line
	2900 1950 3550 1950
Connection ~ 2900 1950
Wire Wire Line
	2900 1950 2900 2100
$Comp
L Relay:ADW11 K?
U 1 1 60BD1A44
P 3850 2550
F 0 "K?" V 3283 2550 50  0000 C CNN
F 1 "ADW11" V 3374 2550 50  0000 C CNN
F 2 "Relay_THT:Relay_1P1T_NO_10x24x18.8mm_Panasonic_ADW11xxxxW_THT" H 5175 2500 50  0001 C CNN
F 3 "https://www.panasonic-electric-works.com/pew/es/downloads/ds_dw_hl_en.pdf" H 3850 2550 50  0001 C CNN
	1    3850 2550
	0    1    1    0   
$EndComp
Wire Wire Line
	4300 1950 4300 2850
Wire Wire Line
	3550 2750 3550 3550
Wire Wire Line
	4150 2850 4300 2850
Connection ~ 4300 2850
Wire Wire Line
	4300 2850 4300 3300
Wire Wire Line
	3550 2350 3550 1950
Connection ~ 3550 1950
Wire Wire Line
	3550 1950 4300 1950
$EndSCHEMATC
