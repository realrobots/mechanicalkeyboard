EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Switch:SW_Push SW1
U 1 1 60A8A101
P 1700 2250
F 0 "SW1" H 1700 2100 50  0000 C CNN
F 1 "Key" H 1700 2200 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 1700 2450 50  0001 C CNN
F 3 "~" H 1700 2450 50  0001 C CNN
	1    1700 2250
	1    0    0    -1  
$EndComp
Text HLabel 6550 3000 2    50   Input ~ 0
ROW1
Text HLabel 6550 3450 2    50   Input ~ 0
ROW2
Text HLabel 2100 1750 1    50   Input ~ 0
COL1
Text HLabel 2700 1750 1    50   Input ~ 0
COL2
Wire Wire Line
	1500 2250 1500 1750
$Comp
L Device:D D1
U 1 1 60A8AC46
P 1900 2400
F 0 "D1" V 1946 2320 50  0000 R CNN
F 1 "D" V 1855 2320 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1900 2400 50  0001 C CNN
F 3 "~" H 1900 2400 50  0001 C CNN
	1    1900 2400
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW9
U 1 1 60AC13C2
P 2300 2250
F 0 "SW9" H 2300 2100 50  0000 C CNN
F 1 "Key" H 2300 2200 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.50u_Plate" H 2300 2450 50  0001 C CNN
F 3 "~" H 2300 2450 50  0001 C CNN
	1    2300 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:D D9
U 1 1 60AC1432
P 2500 2400
F 0 "D9" V 2546 2320 50  0000 R CNN
F 1 "D" V 2455 2320 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2500 2400 50  0001 C CNN
F 3 "~" H 2500 2400 50  0001 C CNN
	1    2500 2400
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW17
U 1 1 60AC63AE
P 2900 2250
F 0 "SW17" H 2900 2100 50  0000 C CNN
F 1 "Key" H 2900 2200 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.75u_Plate" H 2900 2450 50  0001 C CNN
F 3 "~" H 2900 2450 50  0001 C CNN
	1    2900 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:D D17
U 1 1 60AC6472
P 3100 2400
F 0 "D17" V 3146 2320 50  0000 R CNN
F 1 "D" V 3055 2320 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3100 2400 50  0001 C CNN
F 3 "~" H 3100 2400 50  0001 C CNN
	1    3100 2400
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW25
U 1 1 60AC647C
P 3500 2250
F 0 "SW25" H 3500 2100 50  0000 C CNN
F 1 "Key" H 3500 2200 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_2.25u_Plate" H 3500 2450 50  0001 C CNN
F 3 "~" H 3500 2450 50  0001 C CNN
	1    3500 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:D D25
U 1 1 60AC6486
P 3700 2400
F 0 "D25" V 3746 2320 50  0000 R CNN
F 1 "D" V 3655 2320 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3700 2400 50  0001 C CNN
F 3 "~" H 3700 2400 50  0001 C CNN
	1    3700 2400
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW33
U 1 1 60ACE3EE
P 4100 2250
F 0 "SW33" H 4100 2100 50  0000 C CNN
F 1 "Key" H 4100 2200 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.25u_Plate" H 4100 2450 50  0001 C CNN
F 3 "~" H 4100 2450 50  0001 C CNN
	1    4100 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:D D33
U 1 1 60ACE506
P 4300 2400
F 0 "D33" V 4346 2320 50  0000 R CNN
F 1 "D" V 4255 2320 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4300 2400 50  0001 C CNN
F 3 "~" H 4300 2400 50  0001 C CNN
	1    4300 2400
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW41
U 1 1 60ACE510
P 4700 2250
F 0 "SW41" H 4700 2100 50  0000 C CNN
F 1 "Key" H 4700 2200 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 4700 2450 50  0001 C CNN
F 3 "~" H 4700 2450 50  0001 C CNN
	1    4700 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:D D41
U 1 1 60ACE51A
P 4900 2400
F 0 "D41" V 4946 2320 50  0000 R CNN
F 1 "D" V 4855 2320 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4900 2400 50  0001 C CNN
F 3 "~" H 4900 2400 50  0001 C CNN
	1    4900 2400
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW49
U 1 1 60ACE524
P 5300 2250
F 0 "SW49" H 5300 2100 50  0000 C CNN
F 1 "Key" H 5300 2200 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 5300 2450 50  0001 C CNN
F 3 "~" H 5300 2450 50  0001 C CNN
	1    5300 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:D D49
U 1 1 60ACE52E
P 5500 2400
F 0 "D49" V 5546 2320 50  0000 R CNN
F 1 "D" V 5455 2320 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5500 2400 50  0001 C CNN
F 3 "~" H 5500 2400 50  0001 C CNN
	1    5500 2400
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 60ACE538
P 1700 2700
F 0 "SW2" H 1700 2550 50  0000 C CNN
F 1 "Key" H 1700 2650 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 1700 2900 50  0001 C CNN
F 3 "~" H 1700 2900 50  0001 C CNN
	1    1700 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:D D2
U 1 1 60ACE542
P 1900 2850
F 0 "D2" V 1946 2770 50  0000 R CNN
F 1 "D" V 1855 2770 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1900 2850 50  0001 C CNN
F 3 "~" H 1900 2850 50  0001 C CNN
	1    1900 2850
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW10
U 1 1 60ADB594
P 2300 2700
F 0 "SW10" H 2300 2550 50  0000 C CNN
F 1 "Key" H 2300 2650 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 2300 2900 50  0001 C CNN
F 3 "~" H 2300 2900 50  0001 C CNN
	1    2300 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:D D10
U 1 1 60ADB754
P 2500 2850
F 0 "D10" V 2546 2770 50  0000 R CNN
F 1 "D" V 2455 2770 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2500 2850 50  0001 C CNN
F 3 "~" H 2500 2850 50  0001 C CNN
	1    2500 2850
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW18
U 1 1 60ADB75E
P 2900 2700
F 0 "SW18" H 2900 2550 50  0000 C CNN
F 1 "Key" H 2900 2650 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 2900 2900 50  0001 C CNN
F 3 "~" H 2900 2900 50  0001 C CNN
	1    2900 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:D D18
U 1 1 60ADB768
P 3100 2850
F 0 "D18" V 3146 2770 50  0000 R CNN
F 1 "D" V 3055 2770 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3100 2850 50  0001 C CNN
F 3 "~" H 3100 2850 50  0001 C CNN
	1    3100 2850
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW26
U 1 1 60ADB772
P 3500 2700
F 0 "SW26" H 3500 2550 50  0000 C CNN
F 1 "Key" H 3500 2650 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 3500 2900 50  0001 C CNN
F 3 "~" H 3500 2900 50  0001 C CNN
	1    3500 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:D D26
U 1 1 60ADB77C
P 3700 2850
F 0 "D26" V 3746 2770 50  0000 R CNN
F 1 "D" V 3655 2770 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3700 2850 50  0001 C CNN
F 3 "~" H 3700 2850 50  0001 C CNN
	1    3700 2850
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW34
U 1 1 60ADB786
P 4100 2700
F 0 "SW34" H 4100 2550 50  0000 C CNN
F 1 "Key" H 4100 2650 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.25u_Plate" H 4100 2900 50  0001 C CNN
F 3 "~" H 4100 2900 50  0001 C CNN
	1    4100 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:D D34
U 1 1 60ADB790
P 4300 2850
F 0 "D34" V 4346 2770 50  0000 R CNN
F 1 "D" V 4255 2770 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4300 2850 50  0001 C CNN
F 3 "~" H 4300 2850 50  0001 C CNN
	1    4300 2850
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW42
U 1 1 60ADB79A
P 4700 2700
F 0 "SW42" H 4700 2550 50  0000 C CNN
F 1 "Key" H 4700 2650 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 4700 2900 50  0001 C CNN
F 3 "~" H 4700 2900 50  0001 C CNN
	1    4700 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:D D42
U 1 1 60ADB7A4
P 4900 2850
F 0 "D42" V 4946 2770 50  0000 R CNN
F 1 "D" V 4855 2770 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4900 2850 50  0001 C CNN
F 3 "~" H 4900 2850 50  0001 C CNN
	1    4900 2850
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW50
U 1 1 60ADB7AE
P 5300 2700
F 0 "SW50" H 5300 2550 50  0000 C CNN
F 1 "Key" H 5300 2650 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 5300 2900 50  0001 C CNN
F 3 "~" H 5300 2900 50  0001 C CNN
	1    5300 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:D D50
U 1 1 60ADB7B8
P 5500 2850
F 0 "D50" V 5546 2770 50  0000 R CNN
F 1 "D" V 5455 2770 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5500 2850 50  0001 C CNN
F 3 "~" H 5500 2850 50  0001 C CNN
	1    5500 2850
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 60B10A55
P 1700 3150
F 0 "SW3" H 1700 3000 50  0000 C CNN
F 1 "Key" H 1700 3100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 1700 3350 50  0001 C CNN
F 3 "~" H 1700 3350 50  0001 C CNN
	1    1700 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:D D3
U 1 1 60B10CBD
P 1900 3300
F 0 "D3" V 1946 3220 50  0000 R CNN
F 1 "D" V 1855 3220 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1900 3300 50  0001 C CNN
F 3 "~" H 1900 3300 50  0001 C CNN
	1    1900 3300
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW11
U 1 1 60B10CC7
P 2300 3150
F 0 "SW11" H 2300 3000 50  0000 C CNN
F 1 "Key" H 2300 3100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 2300 3350 50  0001 C CNN
F 3 "~" H 2300 3350 50  0001 C CNN
	1    2300 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:D D11
U 1 1 60B10CD1
P 2500 3300
F 0 "D11" V 2546 3220 50  0000 R CNN
F 1 "D" V 2455 3220 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2500 3300 50  0001 C CNN
F 3 "~" H 2500 3300 50  0001 C CNN
	1    2500 3300
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW19
U 1 1 60B10CDB
P 2900 3150
F 0 "SW19" H 2900 3000 50  0000 C CNN
F 1 "Key" H 2900 3100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 2900 3350 50  0001 C CNN
F 3 "~" H 2900 3350 50  0001 C CNN
	1    2900 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:D D19
U 1 1 60B10CE5
P 3100 3300
F 0 "D19" V 3146 3220 50  0000 R CNN
F 1 "D" V 3055 3220 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3100 3300 50  0001 C CNN
F 3 "~" H 3100 3300 50  0001 C CNN
	1    3100 3300
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW27
U 1 1 60B10CEF
P 3500 3150
F 0 "SW27" H 3500 3000 50  0000 C CNN
F 1 "Key" H 3500 3100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 3500 3350 50  0001 C CNN
F 3 "~" H 3500 3350 50  0001 C CNN
	1    3500 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:D D27
U 1 1 60B10CF9
P 3700 3300
F 0 "D27" V 3746 3220 50  0000 R CNN
F 1 "D" V 3655 3220 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3700 3300 50  0001 C CNN
F 3 "~" H 3700 3300 50  0001 C CNN
	1    3700 3300
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW35
U 1 1 60B10D03
P 4100 3150
F 0 "SW35" H 4100 3000 50  0000 C CNN
F 1 "Key" H 4100 3100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.25u_Plate" H 4100 3350 50  0001 C CNN
F 3 "~" H 4100 3350 50  0001 C CNN
	1    4100 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:D D35
U 1 1 60B10D0D
P 4300 3300
F 0 "D35" V 4346 3220 50  0000 R CNN
F 1 "D" V 4255 3220 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4300 3300 50  0001 C CNN
F 3 "~" H 4300 3300 50  0001 C CNN
	1    4300 3300
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW43
U 1 1 60B10D17
P 4700 3150
F 0 "SW43" H 4700 3000 50  0000 C CNN
F 1 "Key" H 4700 3100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 4700 3350 50  0001 C CNN
F 3 "~" H 4700 3350 50  0001 C CNN
	1    4700 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:D D43
U 1 1 60B10D21
P 4900 3300
F 0 "D43" V 4946 3220 50  0000 R CNN
F 1 "D" V 4855 3220 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4900 3300 50  0001 C CNN
F 3 "~" H 4900 3300 50  0001 C CNN
	1    4900 3300
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW51
U 1 1 60B10D2B
P 5300 3150
F 0 "SW51" H 5300 3000 50  0000 C CNN
F 1 "Key" H 5300 3100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 5300 3350 50  0001 C CNN
F 3 "~" H 5300 3350 50  0001 C CNN
	1    5300 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:D D51
U 1 1 60B10D35
P 5500 3300
F 0 "D51" V 5546 3220 50  0000 R CNN
F 1 "D" V 5455 3220 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5500 3300 50  0001 C CNN
F 3 "~" H 5500 3300 50  0001 C CNN
	1    5500 3300
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 60B10D3F
P 1700 3600
F 0 "SW4" H 1700 3450 50  0000 C CNN
F 1 "Key" H 1700 3550 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 1700 3800 50  0001 C CNN
F 3 "~" H 1700 3800 50  0001 C CNN
	1    1700 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:D D4
U 1 1 60B10D49
P 1900 3750
F 0 "D4" V 1946 3670 50  0000 R CNN
F 1 "D" V 1855 3670 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1900 3750 50  0001 C CNN
F 3 "~" H 1900 3750 50  0001 C CNN
	1    1900 3750
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW12
U 1 1 60B10D53
P 2300 3600
F 0 "SW12" H 2300 3450 50  0000 C CNN
F 1 "Key" H 2300 3550 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 2300 3800 50  0001 C CNN
F 3 "~" H 2300 3800 50  0001 C CNN
	1    2300 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:D D12
U 1 1 60B10D5D
P 2500 3750
F 0 "D12" V 2546 3670 50  0000 R CNN
F 1 "D" V 2455 3670 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2500 3750 50  0001 C CNN
F 3 "~" H 2500 3750 50  0001 C CNN
	1    2500 3750
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW20
U 1 1 60B10D67
P 2900 3600
F 0 "SW20" H 2900 3450 50  0000 C CNN
F 1 "Key" H 2900 3550 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 2900 3800 50  0001 C CNN
F 3 "~" H 2900 3800 50  0001 C CNN
	1    2900 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:D D20
U 1 1 60B10D71
P 3100 3750
F 0 "D20" V 3146 3670 50  0000 R CNN
F 1 "D" V 3055 3670 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3100 3750 50  0001 C CNN
F 3 "~" H 3100 3750 50  0001 C CNN
	1    3100 3750
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW28
U 1 1 60B10D7B
P 3500 3600
F 0 "SW28" H 3500 3450 50  0000 C CNN
F 1 "Key" H 3500 3550 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 3500 3800 50  0001 C CNN
F 3 "~" H 3500 3800 50  0001 C CNN
	1    3500 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:D D28
U 1 1 60B10D85
P 3700 3750
F 0 "D28" V 3746 3670 50  0000 R CNN
F 1 "D" V 3655 3670 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3700 3750 50  0001 C CNN
F 3 "~" H 3700 3750 50  0001 C CNN
	1    3700 3750
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW36
U 1 1 60B10D8F
P 4100 3600
F 0 "SW36" H 4100 3450 50  0000 C CNN
F 1 "Key" H 4100 3550 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_6.25u_Plate" H 4100 3800 50  0001 C CNN
F 3 "~" H 4100 3800 50  0001 C CNN
	1    4100 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:D D36
U 1 1 60B10D99
P 4300 3750
F 0 "D36" V 4346 3670 50  0000 R CNN
F 1 "D" V 4255 3670 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4300 3750 50  0001 C CNN
F 3 "~" H 4300 3750 50  0001 C CNN
	1    4300 3750
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW44
U 1 1 60B10DA3
P 4700 3600
F 0 "SW44" H 4700 3450 50  0000 C CNN
F 1 "Key" H 4700 3550 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 4700 3800 50  0001 C CNN
F 3 "~" H 4700 3800 50  0001 C CNN
	1    4700 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:D D44
U 1 1 60B10DAD
P 4900 3750
F 0 "D44" V 4946 3670 50  0000 R CNN
F 1 "D" V 4855 3670 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4900 3750 50  0001 C CNN
F 3 "~" H 4900 3750 50  0001 C CNN
	1    4900 3750
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW52
U 1 1 60B10DB7
P 5300 3600
F 0 "SW52" H 5300 3450 50  0000 C CNN
F 1 "Key" H 5300 3550 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.50u_Plate" H 5300 3800 50  0001 C CNN
F 3 "~" H 5300 3800 50  0001 C CNN
	1    5300 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:D D52
U 1 1 60B10DC1
P 5500 3750
F 0 "D52" V 5546 3670 50  0000 R CNN
F 1 "D" V 5455 3670 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5500 3750 50  0001 C CNN
F 3 "~" H 5500 3750 50  0001 C CNN
	1    5500 3750
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW5
U 1 1 60B28211
P 1700 4050
F 0 "SW5" H 1700 3900 50  0000 C CNN
F 1 "Key" H 1700 4000 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 1700 4250 50  0001 C CNN
F 3 "~" H 1700 4250 50  0001 C CNN
	1    1700 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:D D5
U 1 1 60B286C5
P 1900 4200
F 0 "D5" V 1946 4120 50  0000 R CNN
F 1 "D" V 1855 4120 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1900 4200 50  0001 C CNN
F 3 "~" H 1900 4200 50  0001 C CNN
	1    1900 4200
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW13
U 1 1 60B286CF
P 2300 4050
F 0 "SW13" H 2300 3900 50  0000 C CNN
F 1 "Key" H 2300 4000 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 2300 4250 50  0001 C CNN
F 3 "~" H 2300 4250 50  0001 C CNN
	1    2300 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:D D13
U 1 1 60B286D9
P 2500 4200
F 0 "D13" V 2546 4120 50  0000 R CNN
F 1 "D" V 2455 4120 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2500 4200 50  0001 C CNN
F 3 "~" H 2500 4200 50  0001 C CNN
	1    2500 4200
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW21
U 1 1 60B286E3
P 2900 4050
F 0 "SW21" H 2900 3900 50  0000 C CNN
F 1 "Key" H 2900 4000 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 2900 4250 50  0001 C CNN
F 3 "~" H 2900 4250 50  0001 C CNN
	1    2900 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:D D21
U 1 1 60B286ED
P 3100 4200
F 0 "D21" V 3146 4120 50  0000 R CNN
F 1 "D" V 3055 4120 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3100 4200 50  0001 C CNN
F 3 "~" H 3100 4200 50  0001 C CNN
	1    3100 4200
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW29
U 1 1 60B286F7
P 3500 4050
F 0 "SW29" H 3500 3900 50  0000 C CNN
F 1 "Key" H 3500 4000 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 3500 4250 50  0001 C CNN
F 3 "~" H 3500 4250 50  0001 C CNN
	1    3500 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:D D29
U 1 1 60B28701
P 3700 4200
F 0 "D29" V 3746 4120 50  0000 R CNN
F 1 "D" V 3655 4120 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3700 4200 50  0001 C CNN
F 3 "~" H 3700 4200 50  0001 C CNN
	1    3700 4200
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW37
U 1 1 60B2870B
P 4100 4050
F 0 "SW37" H 4100 3900 50  0000 C CNN
F 1 "Key" H 4100 4000 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.25u_Plate" H 4100 4250 50  0001 C CNN
F 3 "~" H 4100 4250 50  0001 C CNN
	1    4100 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:D D37
U 1 1 60B28715
P 4300 4200
F 0 "D37" V 4346 4120 50  0000 R CNN
F 1 "D" V 4255 4120 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4300 4200 50  0001 C CNN
F 3 "~" H 4300 4200 50  0001 C CNN
	1    4300 4200
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW45
U 1 1 60B2871F
P 4700 4050
F 0 "SW45" H 4700 3900 50  0000 C CNN
F 1 "Key" H 4700 4000 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 4700 4250 50  0001 C CNN
F 3 "~" H 4700 4250 50  0001 C CNN
	1    4700 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:D D45
U 1 1 60B28729
P 4900 4200
F 0 "D45" V 4946 4120 50  0000 R CNN
F 1 "D" V 4855 4120 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4900 4200 50  0001 C CNN
F 3 "~" H 4900 4200 50  0001 C CNN
	1    4900 4200
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW53
U 1 1 60B28733
P 5300 4050
F 0 "SW53" H 5300 3900 50  0000 C CNN
F 1 "Key" H 5300 4000 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 5300 4250 50  0001 C CNN
F 3 "~" H 5300 4250 50  0001 C CNN
	1    5300 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:D D53
U 1 1 60B2873D
P 5500 4200
F 0 "D53" V 5546 4120 50  0000 R CNN
F 1 "D" V 5455 4120 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5500 4200 50  0001 C CNN
F 3 "~" H 5500 4200 50  0001 C CNN
	1    5500 4200
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW6
U 1 1 60B28747
P 1700 4500
F 0 "SW6" H 1700 4350 50  0000 C CNN
F 1 "Key" H 1700 4450 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 1700 4700 50  0001 C CNN
F 3 "~" H 1700 4700 50  0001 C CNN
	1    1700 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:D D6
U 1 1 60B28751
P 1900 4650
F 0 "D6" V 1946 4570 50  0000 R CNN
F 1 "D" V 1855 4570 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1900 4650 50  0001 C CNN
F 3 "~" H 1900 4650 50  0001 C CNN
	1    1900 4650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW14
U 1 1 60B2875B
P 2300 4500
F 0 "SW14" H 2300 4350 50  0000 C CNN
F 1 "Key" H 2300 4450 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 2300 4700 50  0001 C CNN
F 3 "~" H 2300 4700 50  0001 C CNN
	1    2300 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:D D14
U 1 1 60B28765
P 2500 4650
F 0 "D14" V 2546 4570 50  0000 R CNN
F 1 "D" V 2455 4570 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2500 4650 50  0001 C CNN
F 3 "~" H 2500 4650 50  0001 C CNN
	1    2500 4650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW22
U 1 1 60B2876F
P 2900 4500
F 0 "SW22" H 2900 4350 50  0000 C CNN
F 1 "Key" H 2900 4450 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 2900 4700 50  0001 C CNN
F 3 "~" H 2900 4700 50  0001 C CNN
	1    2900 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:D D22
U 1 1 60B28779
P 3100 4650
F 0 "D22" V 3146 4570 50  0000 R CNN
F 1 "D" V 3055 4570 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3100 4650 50  0001 C CNN
F 3 "~" H 3100 4650 50  0001 C CNN
	1    3100 4650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW30
U 1 1 60B28783
P 3500 4500
F 0 "SW30" H 3500 4350 50  0000 C CNN
F 1 "Key" H 3500 4450 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 3500 4700 50  0001 C CNN
F 3 "~" H 3500 4700 50  0001 C CNN
	1    3500 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:D D30
U 1 1 60B2878D
P 3700 4650
F 0 "D30" V 3746 4570 50  0000 R CNN
F 1 "D" V 3655 4570 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3700 4650 50  0001 C CNN
F 3 "~" H 3700 4650 50  0001 C CNN
	1    3700 4650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW38
U 1 1 60B28797
P 4100 4500
F 0 "SW38" H 4100 4350 50  0000 C CNN
F 1 "Key" H 4100 4450 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.25u_Plate" H 4100 4700 50  0001 C CNN
F 3 "~" H 4100 4700 50  0001 C CNN
	1    4100 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:D D38
U 1 1 60B287A1
P 4300 4650
F 0 "D38" V 4346 4570 50  0000 R CNN
F 1 "D" V 4255 4570 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4300 4650 50  0001 C CNN
F 3 "~" H 4300 4650 50  0001 C CNN
	1    4300 4650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW46
U 1 1 60B287AB
P 4700 4500
F 0 "SW46" H 4700 4350 50  0000 C CNN
F 1 "Key" H 4700 4450 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_2.00u_Plate" H 4700 4700 50  0001 C CNN
F 3 "~" H 4700 4700 50  0001 C CNN
	1    4700 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:D D46
U 1 1 60B287B5
P 4900 4650
F 0 "D46" V 4946 4570 50  0000 R CNN
F 1 "D" V 4855 4570 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4900 4650 50  0001 C CNN
F 3 "~" H 4900 4650 50  0001 C CNN
	1    4900 4650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW54
U 1 1 60B287BF
P 5300 4500
F 0 "SW54" H 5300 4350 50  0000 C CNN
F 1 "Key" H 5300 4450 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 5300 4700 50  0001 C CNN
F 3 "~" H 5300 4700 50  0001 C CNN
	1    5300 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:D D54
U 1 1 60B287C9
P 5500 4650
F 0 "D54" V 5546 4570 50  0000 R CNN
F 1 "D" V 5455 4570 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5500 4650 50  0001 C CNN
F 3 "~" H 5500 4650 50  0001 C CNN
	1    5500 4650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW7
U 1 1 60B287D3
P 1700 4950
F 0 "SW7" H 1700 4800 50  0000 C CNN
F 1 "Key" H 1700 4900 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 1700 5150 50  0001 C CNN
F 3 "~" H 1700 5150 50  0001 C CNN
	1    1700 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:D D7
U 1 1 60B287DD
P 1900 5100
F 0 "D7" V 1946 5020 50  0000 R CNN
F 1 "D" V 1855 5020 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1900 5100 50  0001 C CNN
F 3 "~" H 1900 5100 50  0001 C CNN
	1    1900 5100
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW15
U 1 1 60B287E7
P 2300 4950
F 0 "SW15" H 2300 4800 50  0000 C CNN
F 1 "Key" H 2300 4900 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 2300 5150 50  0001 C CNN
F 3 "~" H 2300 5150 50  0001 C CNN
	1    2300 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:D D15
U 1 1 60B287F1
P 2500 5100
F 0 "D15" V 2546 5020 50  0000 R CNN
F 1 "D" V 2455 5020 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2500 5100 50  0001 C CNN
F 3 "~" H 2500 5100 50  0001 C CNN
	1    2500 5100
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW23
U 1 1 60B287FB
P 2900 4950
F 0 "SW23" H 2900 4800 50  0000 C CNN
F 1 "Key" H 2900 4900 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 2900 5150 50  0001 C CNN
F 3 "~" H 2900 5150 50  0001 C CNN
	1    2900 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:D D23
U 1 1 60B28805
P 3100 5100
F 0 "D23" V 3146 5020 50  0000 R CNN
F 1 "D" V 3055 5020 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3100 5100 50  0001 C CNN
F 3 "~" H 3100 5100 50  0001 C CNN
	1    3100 5100
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW31
U 1 1 60B2880F
P 3500 4950
F 0 "SW31" H 3500 4800 50  0000 C CNN
F 1 "Key" H 3500 4900 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 3500 5150 50  0001 C CNN
F 3 "~" H 3500 5150 50  0001 C CNN
	1    3500 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:D D31
U 1 1 60B28819
P 3700 5100
F 0 "D31" V 3746 5020 50  0000 R CNN
F 1 "D" V 3655 5020 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3700 5100 50  0001 C CNN
F 3 "~" H 3700 5100 50  0001 C CNN
	1    3700 5100
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW39
U 1 1 60B28823
P 4100 4950
F 0 "SW39" H 4100 4800 50  0000 C CNN
F 1 "Key" H 4100 4900 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.25u_Plate" H 4100 5150 50  0001 C CNN
F 3 "~" H 4100 5150 50  0001 C CNN
	1    4100 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:D D39
U 1 1 60B2882D
P 4300 5100
F 0 "D39" V 4346 5020 50  0000 R CNN
F 1 "D" V 4255 5020 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4300 5100 50  0001 C CNN
F 3 "~" H 4300 5100 50  0001 C CNN
	1    4300 5100
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW47
U 1 1 60B28837
P 4700 4950
F 0 "SW47" H 4700 4800 50  0000 C CNN
F 1 "Key" H 4700 4900 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 4700 5150 50  0001 C CNN
F 3 "~" H 4700 5150 50  0001 C CNN
	1    4700 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:D D47
U 1 1 60B28841
P 4900 5100
F 0 "D47" V 4946 5020 50  0000 R CNN
F 1 "D" V 4855 5020 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4900 5100 50  0001 C CNN
F 3 "~" H 4900 5100 50  0001 C CNN
	1    4900 5100
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW55
U 1 1 60B2884B
P 5300 4950
F 0 "SW55" H 5300 4800 50  0000 C CNN
F 1 "Key" H 5300 4900 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_2.25u_Plate" H 5300 5150 50  0001 C CNN
F 3 "~" H 5300 5150 50  0001 C CNN
	1    5300 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:D D55
U 1 1 60B28855
P 5500 5100
F 0 "D55" V 5546 5020 50  0000 R CNN
F 1 "D" V 5455 5020 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5500 5100 50  0001 C CNN
F 3 "~" H 5500 5100 50  0001 C CNN
	1    5500 5100
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW8
U 1 1 60B2885F
P 1700 5400
F 0 "SW8" H 1700 5250 50  0000 C CNN
F 1 "Key" H 1700 5350 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 1700 5600 50  0001 C CNN
F 3 "~" H 1700 5600 50  0001 C CNN
	1    1700 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:D D8
U 1 1 60B28869
P 1900 5550
F 0 "D8" V 1946 5470 50  0000 R CNN
F 1 "D" V 1855 5470 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1900 5550 50  0001 C CNN
F 3 "~" H 1900 5550 50  0001 C CNN
	1    1900 5550
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW16
U 1 1 60B28873
P 2300 5400
F 0 "SW16" H 2300 5250 50  0000 C CNN
F 1 "Key" H 2300 5350 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 2300 5600 50  0001 C CNN
F 3 "~" H 2300 5600 50  0001 C CNN
	1    2300 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:D D16
U 1 1 60B2887D
P 2500 5550
F 0 "D16" V 2546 5470 50  0000 R CNN
F 1 "D" V 2455 5470 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2500 5550 50  0001 C CNN
F 3 "~" H 2500 5550 50  0001 C CNN
	1    2500 5550
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW24
U 1 1 60B28887
P 2900 5400
F 0 "SW24" H 2900 5250 50  0000 C CNN
F 1 "Key" H 2900 5350 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 2900 5600 50  0001 C CNN
F 3 "~" H 2900 5600 50  0001 C CNN
	1    2900 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:D D24
U 1 1 60B28891
P 3100 5550
F 0 "D24" V 3146 5470 50  0000 R CNN
F 1 "D" V 3055 5470 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3100 5550 50  0001 C CNN
F 3 "~" H 3100 5550 50  0001 C CNN
	1    3100 5550
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW32
U 1 1 60B2889B
P 3500 5400
F 0 "SW32" H 3500 5250 50  0000 C CNN
F 1 "Key" H 3500 5350 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 3500 5600 50  0001 C CNN
F 3 "~" H 3500 5600 50  0001 C CNN
	1    3500 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:D D32
U 1 1 60B288A5
P 3700 5550
F 0 "D32" V 3746 5470 50  0000 R CNN
F 1 "D" V 3655 5470 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3700 5550 50  0001 C CNN
F 3 "~" H 3700 5550 50  0001 C CNN
	1    3700 5550
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW40
U 1 1 60B288AF
P 4100 5400
F 0 "SW40" H 4100 5250 50  0000 C CNN
F 1 "Key" H 4100 5350 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.25u_Plate" H 4100 5600 50  0001 C CNN
F 3 "~" H 4100 5600 50  0001 C CNN
	1    4100 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:D D40
U 1 1 60B288B9
P 4300 5550
F 0 "D40" V 4346 5470 50  0000 R CNN
F 1 "D" V 4255 5470 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4300 5550 50  0001 C CNN
F 3 "~" H 4300 5550 50  0001 C CNN
	1    4300 5550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1500 2250 1500 2700
Connection ~ 1500 2250
Wire Wire Line
	1500 2700 1500 3150
Connection ~ 1500 2700
Wire Wire Line
	1500 3150 1500 3600
Connection ~ 1500 3150
Wire Wire Line
	1500 3600 1500 4050
Connection ~ 1500 3600
Wire Wire Line
	1500 4050 1500 4500
Connection ~ 1500 4050
Wire Wire Line
	1500 4500 1500 4950
Connection ~ 1500 4500
Wire Wire Line
	1500 4950 1500 5400
Connection ~ 1500 4950
Wire Wire Line
	2100 2250 2100 1750
Wire Wire Line
	2700 2250 2700 1750
Wire Wire Line
	3300 2250 3300 1750
Wire Wire Line
	3900 2250 3900 1750
Wire Wire Line
	4500 2250 4500 1750
Wire Wire Line
	5100 2250 5100 1750
$Comp
L Switch:SW_Push SW56
U 1 1 60BB24CF
P 5900 2250
F 0 "SW56" H 5900 2100 50  0000 C CNN
F 1 "Key" H 5900 2200 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 5900 2450 50  0001 C CNN
F 3 "~" H 5900 2450 50  0001 C CNN
	1    5900 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:D D56
U 1 1 60BB2DC7
P 6100 2400
F 0 "D56" V 6146 2320 50  0000 R CNN
F 1 "D" V 6055 2320 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6100 2400 50  0001 C CNN
F 3 "~" H 6100 2400 50  0001 C CNN
	1    6100 2400
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW57
U 1 1 60BB2DD1
P 5900 2700
F 0 "SW57" H 5900 2550 50  0000 C CNN
F 1 "Key" H 5900 2650 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 5900 2900 50  0001 C CNN
F 3 "~" H 5900 2900 50  0001 C CNN
	1    5900 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:D D57
U 1 1 60BB2DDB
P 6100 2850
F 0 "D57" V 6146 2770 50  0000 R CNN
F 1 "D" V 6055 2770 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6100 2850 50  0001 C CNN
F 3 "~" H 6100 2850 50  0001 C CNN
	1    6100 2850
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW58
U 1 1 60BB2DE5
P 5900 3150
F 0 "SW58" H 5900 3000 50  0000 C CNN
F 1 "Key" H 5900 3100 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 5900 3350 50  0001 C CNN
F 3 "~" H 5900 3350 50  0001 C CNN
	1    5900 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:D D58
U 1 1 60BB2DEF
P 6100 3300
F 0 "D58" V 6146 3220 50  0000 R CNN
F 1 "D" V 6055 3220 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6100 3300 50  0001 C CNN
F 3 "~" H 6100 3300 50  0001 C CNN
	1    6100 3300
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW59
U 1 1 60BB2DF9
P 5900 3600
F 0 "SW59" H 5900 3450 50  0000 C CNN
F 1 "Key" H 5900 3550 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 5900 3800 50  0001 C CNN
F 3 "~" H 5900 3800 50  0001 C CNN
	1    5900 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:D D59
U 1 1 60BB2E03
P 6100 3750
F 0 "D59" V 6146 3670 50  0000 R CNN
F 1 "D" V 6055 3670 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6100 3750 50  0001 C CNN
F 3 "~" H 6100 3750 50  0001 C CNN
	1    6100 3750
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW60
U 1 1 60BB2E0D
P 5900 4050
F 0 "SW60" H 5900 3900 50  0000 C CNN
F 1 "Key" H 5900 4000 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 5900 4250 50  0001 C CNN
F 3 "~" H 5900 4250 50  0001 C CNN
	1    5900 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:D D60
U 1 1 60BB2E17
P 6100 4200
F 0 "D60" V 6146 4120 50  0000 R CNN
F 1 "D" V 6055 4120 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6100 4200 50  0001 C CNN
F 3 "~" H 6100 4200 50  0001 C CNN
	1    6100 4200
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW61
U 1 1 60BB2E21
P 5900 4500
F 0 "SW61" H 5900 4350 50  0000 C CNN
F 1 "Key" H 5900 4450 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 5900 4700 50  0001 C CNN
F 3 "~" H 5900 4700 50  0001 C CNN
	1    5900 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:D D61
U 1 1 60BB2E2B
P 6100 4650
F 0 "D61" V 6146 4570 50  0000 R CNN
F 1 "D" V 6055 4570 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6100 4650 50  0001 C CNN
F 3 "~" H 6100 4650 50  0001 C CNN
	1    6100 4650
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW62
U 1 1 60BB2E35
P 5900 4950
F 0 "SW62" H 5900 4800 50  0000 C CNN
F 1 "Key" H 5900 4900 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.25u_Plate" H 5900 5150 50  0001 C CNN
F 3 "~" H 5900 5150 50  0001 C CNN
	1    5900 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:D D62
U 1 1 60BB2E3F
P 6100 5100
F 0 "D62" V 6146 5020 50  0000 R CNN
F 1 "D" V 6055 5020 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6100 5100 50  0001 C CNN
F 3 "~" H 6100 5100 50  0001 C CNN
	1    6100 5100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5700 2250 5700 1750
$Comp
L Switch:SW_Push SW48
U 1 1 60BCFDFA
P 4700 5400
F 0 "SW48" H 4700 5250 50  0000 C CNN
F 1 "Key" H 4700 5350 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 4700 5600 50  0001 C CNN
F 3 "~" H 4700 5600 50  0001 C CNN
	1    4700 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:D D48
U 1 1 60BD0818
P 4900 5550
F 0 "D48" V 4946 5470 50  0000 R CNN
F 1 "D" V 4855 5470 50  0000 R CNN
F 2 "MechanicalKeyboard:D_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4900 5550 50  0001 C CNN
F 3 "~" H 4900 5550 50  0001 C CNN
	1    4900 5550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2100 2250 2100 2700
Connection ~ 2100 2250
Wire Wire Line
	2100 2700 2100 3150
Connection ~ 2100 2700
Wire Wire Line
	2100 3150 2100 3600
Connection ~ 2100 3150
Wire Wire Line
	2100 3600 2100 4050
Connection ~ 2100 3600
Wire Wire Line
	2100 4050 2100 4500
Connection ~ 2100 4050
Wire Wire Line
	2100 4500 2100 4950
Connection ~ 2100 4500
Wire Wire Line
	2100 4950 2100 5400
Connection ~ 2100 4950
Wire Wire Line
	2700 5400 2700 4950
Wire Wire Line
	2700 4950 2700 4500
Connection ~ 2700 4950
Wire Wire Line
	2700 4500 2700 4050
Connection ~ 2700 4500
Wire Wire Line
	3300 4050 3300 4500
Wire Wire Line
	3300 4500 3300 4950
Connection ~ 3300 4500
Wire Wire Line
	3300 4950 3300 5400
Connection ~ 3300 4950
Wire Wire Line
	3900 5400 3900 4950
Wire Wire Line
	3900 4500 3900 4950
Connection ~ 3900 4950
Connection ~ 3900 4500
Wire Wire Line
	2700 4050 2700 3600
Connection ~ 2700 4050
Wire Wire Line
	2700 3600 2700 3150
Connection ~ 2700 3600
Wire Wire Line
	2700 3150 2700 2700
Connection ~ 2700 3150
Wire Wire Line
	2700 2700 2700 2250
Connection ~ 2700 2700
Connection ~ 2700 2250
Wire Wire Line
	3300 2250 3300 2700
Connection ~ 3300 2250
Wire Wire Line
	3300 2700 3300 3150
Connection ~ 3300 2700
Wire Wire Line
	3300 3150 3300 3600
Connection ~ 3300 3150
Wire Wire Line
	3300 4050 3300 3600
Connection ~ 3300 4050
Connection ~ 3300 3600
Wire Wire Line
	3900 2250 3900 2700
Connection ~ 3900 2250
Wire Wire Line
	3900 2700 3900 3150
Connection ~ 3900 2700
Wire Wire Line
	3900 3150 3900 3600
Connection ~ 3900 3150
Wire Wire Line
	3900 3600 3900 4050
Connection ~ 3900 3600
Connection ~ 3900 4050
Wire Wire Line
	3900 4050 3900 4500
Wire Wire Line
	4500 2250 4500 2700
Connection ~ 4500 2250
Connection ~ 4500 2700
Wire Wire Line
	4500 2700 4500 3150
Wire Wire Line
	4500 3150 4500 3600
Connection ~ 4500 3150
Wire Wire Line
	4500 3600 4500 4050
Connection ~ 4500 3600
Wire Wire Line
	4500 4050 4500 4500
Connection ~ 4500 4050
Wire Wire Line
	4500 4500 4500 4950
Connection ~ 4500 4500
Wire Wire Line
	4500 4950 4500 5400
Connection ~ 4500 4950
Wire Wire Line
	5100 4950 5100 4500
Wire Wire Line
	5100 4500 5100 4050
Connection ~ 5100 4500
Wire Wire Line
	5100 4050 5100 3600
Connection ~ 5100 4050
Wire Wire Line
	5100 3600 5100 3150
Connection ~ 5100 3600
Wire Wire Line
	5100 3150 5100 2700
Connection ~ 5100 3150
Wire Wire Line
	5100 2700 5100 2250
Connection ~ 5100 2700
Connection ~ 5100 2250
Wire Wire Line
	5700 2250 5700 2700
Connection ~ 5700 2250
Wire Wire Line
	5700 2700 5700 3150
Connection ~ 5700 2700
Wire Wire Line
	5700 3150 5700 3600
Connection ~ 5700 3150
Wire Wire Line
	5700 3600 5700 4050
Connection ~ 5700 3600
Wire Wire Line
	5700 4050 5700 4500
Connection ~ 5700 4050
Wire Wire Line
	5700 4500 5700 4950
Connection ~ 5700 4500
Wire Wire Line
	1900 5700 2500 5700
Wire Wire Line
	2500 5700 3100 5700
Connection ~ 2500 5700
Wire Wire Line
	3100 5700 3700 5700
Connection ~ 3100 5700
Wire Wire Line
	3700 5700 4300 5700
Connection ~ 3700 5700
Wire Wire Line
	4300 5700 4900 5700
Connection ~ 4300 5700
Wire Wire Line
	1900 5250 2500 5250
Wire Wire Line
	2500 5250 3100 5250
Connection ~ 2500 5250
Wire Wire Line
	3100 5250 3700 5250
Connection ~ 3100 5250
Wire Wire Line
	3700 5250 4300 5250
Connection ~ 3700 5250
Wire Wire Line
	4300 5250 4900 5250
Connection ~ 4300 5250
Wire Wire Line
	4900 5250 5500 5250
Connection ~ 4900 5250
Wire Wire Line
	5500 5250 6100 5250
Connection ~ 5500 5250
Wire Wire Line
	1900 4800 2500 4800
Wire Wire Line
	2500 4800 3100 4800
Connection ~ 2500 4800
Wire Wire Line
	3100 4800 3700 4800
Connection ~ 3100 4800
Wire Wire Line
	3700 4800 4300 4800
Connection ~ 3700 4800
Wire Wire Line
	4300 4800 4900 4800
Connection ~ 4300 4800
Wire Wire Line
	4900 4800 5500 4800
Connection ~ 4900 4800
Wire Wire Line
	5500 4800 6100 4800
Connection ~ 5500 4800
Wire Wire Line
	1900 4350 2500 4350
Wire Wire Line
	2500 4350 3100 4350
Connection ~ 2500 4350
Wire Wire Line
	3100 4350 3700 4350
Connection ~ 3100 4350
Wire Wire Line
	3700 4350 4300 4350
Connection ~ 3700 4350
Wire Wire Line
	4300 4350 4900 4350
Connection ~ 4300 4350
Wire Wire Line
	4900 4350 5500 4350
Connection ~ 4900 4350
Wire Wire Line
	5500 4350 6100 4350
Connection ~ 5500 4350
Wire Wire Line
	1900 3900 2500 3900
Wire Wire Line
	2500 3900 3100 3900
Connection ~ 2500 3900
Wire Wire Line
	3100 3900 3700 3900
Connection ~ 3100 3900
Wire Wire Line
	3700 3900 4300 3900
Connection ~ 3700 3900
Wire Wire Line
	4300 3900 4900 3900
Connection ~ 4300 3900
Wire Wire Line
	4900 3900 5500 3900
Connection ~ 4900 3900
Wire Wire Line
	5500 3900 6100 3900
Connection ~ 5500 3900
Wire Wire Line
	1900 3450 2500 3450
Wire Wire Line
	2500 3450 3100 3450
Connection ~ 2500 3450
Wire Wire Line
	3100 3450 3700 3450
Connection ~ 3100 3450
Wire Wire Line
	3700 3450 4300 3450
Connection ~ 3700 3450
Wire Wire Line
	4300 3450 4900 3450
Connection ~ 4300 3450
Wire Wire Line
	4900 3450 5500 3450
Connection ~ 4900 3450
Wire Wire Line
	5500 3450 6100 3450
Connection ~ 5500 3450
Wire Wire Line
	1900 3000 2500 3000
Wire Wire Line
	2500 3000 3100 3000
Connection ~ 2500 3000
Wire Wire Line
	3100 3000 3700 3000
Connection ~ 3100 3000
Wire Wire Line
	3700 3000 4300 3000
Connection ~ 3700 3000
Wire Wire Line
	4300 3000 4900 3000
Connection ~ 4300 3000
Wire Wire Line
	4900 3000 5500 3000
Connection ~ 4900 3000
Wire Wire Line
	5500 3000 6100 3000
Connection ~ 5500 3000
Wire Wire Line
	1900 2550 2500 2550
Wire Wire Line
	2500 2550 3100 2550
Connection ~ 2500 2550
Wire Wire Line
	3100 2550 3700 2550
Connection ~ 3100 2550
Wire Wire Line
	3700 2550 4300 2550
Connection ~ 3700 2550
Wire Wire Line
	4300 2550 4900 2550
Connection ~ 4300 2550
Wire Wire Line
	4900 2550 5500 2550
Connection ~ 4900 2550
Wire Wire Line
	5500 2550 6100 2550
Connection ~ 5500 2550
Wire Wire Line
	6100 2550 6550 2550
Connection ~ 6100 2550
Wire Wire Line
	6100 3000 6550 3000
Connection ~ 6100 3000
Wire Wire Line
	6100 3450 6550 3450
Connection ~ 6100 3450
Wire Wire Line
	6100 3900 6550 3900
Connection ~ 6100 3900
Wire Wire Line
	6100 4350 6550 4350
Connection ~ 6100 4350
Wire Wire Line
	6100 4800 6550 4800
Connection ~ 6100 4800
Wire Wire Line
	6100 5250 6550 5250
Connection ~ 6100 5250
Text HLabel 1500 1750 1    50   Input ~ 0
COL0
Text HLabel 3900 1750 1    50   Input ~ 0
COL4
Text HLabel 4500 1750 1    50   Input ~ 0
COL5
Text HLabel 3300 1750 1    50   Input ~ 0
COL3
Text HLabel 5100 1750 1    50   Input ~ 0
COL6
Text HLabel 5700 1750 1    50   Input ~ 0
COL7
Text HLabel 6550 2550 2    50   Input ~ 0
ROW0
Text HLabel 6550 3900 2    50   Input ~ 0
ROW3
Text HLabel 6550 4350 2    50   Input ~ 0
ROW4
Text HLabel 6550 4800 2    50   Input ~ 0
ROW5
Text HLabel 6550 5250 2    50   Input ~ 0
ROW6
Text HLabel 6550 5700 2    50   Input ~ 0
ROW7
Wire Wire Line
	4900 5700 6550 5700
Connection ~ 4900 5700
$Comp
L MechanicalKeyboard:ArduinoProMicro U2
U 1 1 60AD1B98
P 8900 1700
F 0 "U2" H 8875 1765 50  0000 C CNN
F 1 "ArduinoProMicro" H 8875 1674 50  0000 C CNN
F 2 "MechanicalKeyboard:Arduino_Pro_Micro" H 8900 1700 50  0001 C CNN
F 3 "" H 8900 1700 50  0001 C CNN
	1    8900 1700
	1    0    0    -1  
$EndComp
$Comp
L Interface_Expansion:MCP23017_SP U1
U 1 1 60AD5251
P 8850 5000
F 0 "U1" H 8850 6281 50  0000 C CNN
F 1 "MCP23017_SP" H 8850 6190 50  0000 C CNN
F 2 "Package_DIP:DIP-28_W7.62mm" H 9050 4000 50  0001 L CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20001952C.pdf" H 9050 3900 50  0001 L CNN
	1    8850 5000
	1    0    0    -1  
$EndComp
Text HLabel 9300 2300 2    50   Input ~ 0
COL0
Text HLabel 9300 2400 2    50   Input ~ 0
COL1
Text HLabel 9300 2500 2    50   Input ~ 0
COL2
Text HLabel 9300 2600 2    50   Input ~ 0
COL3
Text HLabel 9300 2700 2    50   Input ~ 0
COL4
Text HLabel 9300 2800 2    50   Input ~ 0
COL5
Text HLabel 9300 2900 2    50   Input ~ 0
COL6
Text HLabel 9300 3000 2    50   Input ~ 0
COL7
Text HLabel 9550 5100 2    50   Input ~ 0
ROW0
Text HLabel 9550 5200 2    50   Input ~ 0
ROW1
Text HLabel 9550 5300 2    50   Input ~ 0
ROW2
Text HLabel 9550 5400 2    50   Input ~ 0
ROW3
Text HLabel 9550 5500 2    50   Input ~ 0
ROW4
Text HLabel 9550 5600 2    50   Input ~ 0
ROW5
Text HLabel 9550 5700 2    50   Input ~ 0
ROW6
Text HLabel 9550 5800 2    50   Input ~ 0
ROW7
$Comp
L Device:R_Small R1
U 1 1 60B09E97
P 7750 2550
F 0 "R1" V 7554 2550 50  0000 C CNN
F 1 "4.7k" V 7645 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 7750 2550 50  0001 C CNN
F 3 "~" H 7750 2550 50  0001 C CNN
	1    7750 2550
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R2
U 1 1 60B0EFF0
P 7750 2900
F 0 "R2" V 7554 2900 50  0000 C CNN
F 1 "4.7k" V 7645 2900 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 7750 2900 50  0001 C CNN
F 3 "~" H 7750 2900 50  0001 C CNN
	1    7750 2900
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0101
U 1 1 60B113E3
P 7400 2500
F 0 "#PWR0101" H 7400 2350 50  0001 C CNN
F 1 "+5V" H 7415 2673 50  0000 C CNN
F 2 "" H 7400 2500 50  0001 C CNN
F 3 "" H 7400 2500 50  0001 C CNN
	1    7400 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 2550 7400 2550
Wire Wire Line
	7400 2550 7400 2500
Wire Wire Line
	7650 2900 7400 2900
Wire Wire Line
	7400 2900 7400 2550
Connection ~ 7400 2550
$Comp
L power:+5V #PWR0102
U 1 1 60B2430E
P 9500 2200
F 0 "#PWR0102" H 9500 2050 50  0001 C CNN
F 1 "+5V" H 9515 2373 50  0000 C CNN
F 2 "" H 9500 2200 50  0001 C CNN
F 3 "" H 9500 2200 50  0001 C CNN
	1    9500 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 2200 9500 2200
$Comp
L power:GND #PWR0103
U 1 1 60B29512
P 8150 2050
F 0 "#PWR0103" H 8150 1800 50  0001 C CNN
F 1 "GND" H 8155 1877 50  0000 C CNN
F 2 "" H 8150 2050 50  0001 C CNN
F 3 "" H 8150 2050 50  0001 C CNN
	1    8150 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 2100 8250 2100
Wire Wire Line
	8250 2100 8250 2050
Wire Wire Line
	8250 2050 8150 2050
Wire Wire Line
	8250 2100 8250 2200
Wire Wire Line
	8250 2200 8450 2200
Connection ~ 8250 2100
$Comp
L power:+5V #PWR0104
U 1 1 60B368B0
P 9200 3900
F 0 "#PWR0104" H 9200 3750 50  0001 C CNN
F 1 "+5V" H 9215 4073 50  0000 C CNN
F 2 "" H 9200 3900 50  0001 C CNN
F 3 "" H 9200 3900 50  0001 C CNN
	1    9200 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 3900 9200 3900
$Comp
L power:GND #PWR0105
U 1 1 60B3F87E
P 8750 6100
F 0 "#PWR0105" H 8750 5850 50  0001 C CNN
F 1 "GND" H 8755 5927 50  0000 C CNN
F 2 "" H 8750 6100 50  0001 C CNN
F 3 "" H 8750 6100 50  0001 C CNN
	1    8750 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 6100 8850 6100
$Comp
L power:GND #PWR0106
U 1 1 60B49424
P 8050 5900
F 0 "#PWR0106" H 8050 5650 50  0001 C CNN
F 1 "GND" H 8055 5727 50  0000 C CNN
F 2 "" H 8050 5900 50  0001 C CNN
F 3 "" H 8050 5900 50  0001 C CNN
	1    8050 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 5600 8050 5700
Wire Wire Line
	8050 5600 8150 5600
Wire Wire Line
	8150 5700 8050 5700
Connection ~ 8050 5700
Wire Wire Line
	8050 5700 8050 5800
Wire Wire Line
	8150 5800 8050 5800
Connection ~ 8050 5800
Wire Wire Line
	8050 5800 8050 5900
$Comp
L power:+5V #PWR0107
U 1 1 60B69A42
P 7700 5000
F 0 "#PWR0107" H 7700 4850 50  0001 C CNN
F 1 "+5V" H 7715 5173 50  0000 C CNN
F 2 "" H 7700 5000 50  0001 C CNN
F 3 "" H 7700 5000 50  0001 C CNN
	1    7700 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 5000 7700 5100
Wire Wire Line
	7700 5100 8150 5100
$Comp
L MechanicalKeyboard:OLED_I2C_128*32 U3
U 1 1 60B94071
P 9200 3400
F 0 "U3" H 9200 3500 50  0000 L CNN
F 1 "OLED_I2C_128*32" H 8950 3400 50  0000 L CNN
F 2 "MechanicalKeyboard:OLED_I2C_128x32" H 9200 3700 50  0001 C CNN
F 3 "" H 9200 3700 50  0001 C CNN
	1    9200 3400
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0108
U 1 1 60B971C6
P 8500 3350
F 0 "#PWR0108" H 8500 3200 50  0001 C CNN
F 1 "+5V" V 8515 3478 50  0000 L CNN
F 2 "" H 8500 3350 50  0001 C CNN
F 3 "" H 8500 3350 50  0001 C CNN
	1    8500 3350
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 60B97DF3
P 8500 3250
F 0 "#PWR0109" H 8500 3000 50  0001 C CNN
F 1 "GND" V 8505 3122 50  0000 R CNN
F 2 "" H 8500 3250 50  0001 C CNN
F 3 "" H 8500 3250 50  0001 C CNN
	1    8500 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	8500 3250 8650 3250
Wire Wire Line
	8500 3350 8650 3350
$Comp
L power:GND #PWR0110
U 1 1 60BF4A0F
P 10650 2750
F 0 "#PWR0110" H 10650 2500 50  0001 C CNN
F 1 "GND" H 10655 2577 50  0000 C CNN
F 2 "" H 10650 2750 50  0001 C CNN
F 3 "" H 10650 2750 50  0001 C CNN
	1    10650 2750
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0111
U 1 1 60BF5493
P 10650 2450
F 0 "#PWR0111" H 10650 2300 50  0001 C CNN
F 1 "+5V" H 10665 2623 50  0000 C CNN
F 2 "" H 10650 2450 50  0001 C CNN
F 3 "" H 10650 2450 50  0001 C CNN
	1    10650 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	10400 2450 10500 2450
Wire Wire Line
	10400 2750 10500 2750
Wire Wire Line
	10400 2650 10500 2650
Wire Wire Line
	10500 2650 10500 2750
Connection ~ 10500 2750
Wire Wire Line
	10500 2750 10650 2750
Wire Wire Line
	10400 2550 10500 2550
Wire Wire Line
	10500 2550 10500 2450
Connection ~ 10500 2450
Wire Wire Line
	10500 2450 10650 2450
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J1
U 1 1 60C6E04F
P 10100 2550
F 0 "J1" H 10150 2867 50  0000 C CNN
F 1 "GPIO HEADER" H 10150 2776 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x04_P2.54mm_Vertical" H 10100 2550 50  0001 C CNN
F 3 "~" H 10100 2550 50  0001 C CNN
	1    10100 2550
	1    0    0    -1  
$EndComp
Text HLabel 9900 2450 0    50   Input ~ 0
D4
Text HLabel 10150 3350 0    50   Input ~ 0
D6
Text HLabel 10250 4350 0    50   Input ~ 0
JC_UD
Text HLabel 10250 4650 0    50   Input ~ 0
JC_LR
$Comp
L Connector:Conn_01x05_Male J2
U 1 1 60D221E0
P 10650 4450
F 0 "J2" H 10622 4382 50  0000 R CNN
F 1 "JOYCON CONN" H 10622 4473 50  0000 R CNN
F 2 "MechanicalKeyboard:XF2W-0515-1A" H 10650 4450 50  0001 C CNN
F 3 "~" H 10650 4450 50  0001 C CNN
	1    10650 4450
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0112
U 1 1 60D2D1F2
P 10450 4250
F 0 "#PWR0112" H 10450 4100 50  0001 C CNN
F 1 "+5V" H 10465 4423 50  0000 C CNN
F 2 "" H 10450 4250 50  0001 C CNN
F 3 "" H 10450 4250 50  0001 C CNN
	1    10450 4250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 60D3434D
P 10450 4550
F 0 "#PWR0113" H 10450 4300 50  0001 C CNN
F 1 "GND" H 10455 4377 50  0000 C CNN
F 2 "" H 10450 4550 50  0001 C CNN
F 3 "" H 10450 4550 50  0001 C CNN
	1    10450 4550
	0    1    1    0   
$EndComp
Text HLabel 10250 4450 0    50   Input ~ 0
JC_BTN
Wire Wire Line
	10250 4350 10450 4350
Wire Wire Line
	10450 4450 10250 4450
Wire Wire Line
	10250 4650 10450 4650
Text HLabel 8450 2800 0    50   Input ~ 0
JC_BTN
Text HLabel 8450 2900 0    50   Input ~ 0
JC_UD
Text HLabel 8450 3000 0    50   Input ~ 0
JC_LR
Wire Wire Line
	8000 2400 8000 2550
Connection ~ 8000 2550
Wire Wire Line
	7850 2550 8000 2550
Wire Wire Line
	8000 2400 8450 2400
Wire Wire Line
	8000 2550 8000 3450
Wire Wire Line
	8050 2300 8050 2900
Wire Wire Line
	8050 4200 8150 4200
Wire Wire Line
	8050 2300 8450 2300
Wire Wire Line
	8650 3550 8050 3550
Connection ~ 8050 3550
Wire Wire Line
	8050 3550 8050 4200
Wire Wire Line
	8000 3450 8650 3450
Wire Wire Line
	8000 3450 8000 4300
Wire Wire Line
	8000 4300 8150 4300
Connection ~ 8000 3450
Wire Wire Line
	7850 2900 8050 2900
Connection ~ 8050 2900
Wire Wire Line
	8050 2900 8050 3550
$Comp
L Device:R_Small R3
U 1 1 60DACD51
P 10350 3250
F 0 "R3" V 10154 3250 50  0000 C CNN
F 1 "1m" V 10245 3250 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 10350 3250 50  0001 C CNN
F 3 "~" H 10350 3250 50  0001 C CNN
	1    10350 3250
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R4
U 1 1 60DB50C2
P 10350 3450
F 0 "R4" V 10154 3450 50  0000 C CNN
F 1 "1m" V 10245 3450 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 10350 3450 50  0001 C CNN
F 3 "~" H 10350 3450 50  0001 C CNN
	1    10350 3450
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 60DCBE6F
P 10150 3550
F 0 "#PWR0114" H 10150 3300 50  0001 C CNN
F 1 "GND" H 10155 3377 50  0000 C CNN
F 2 "" H 10150 3550 50  0001 C CNN
F 3 "" H 10150 3550 50  0001 C CNN
	1    10150 3550
	0    1    1    0   
$EndComp
Wire Wire Line
	10350 2950 10350 3150
Wire Wire Line
	9800 2950 10350 2950
Wire Wire Line
	10350 3550 10150 3550
Wire Wire Line
	10350 3350 10150 3350
Connection ~ 10350 3350
Text HLabel 8450 2700 0    50   Input ~ 0
D6
Wire Wire Line
	9900 2750 9800 2750
Wire Wire Line
	9800 2750 9800 2950
Text HLabel 8450 2500 0    50   Input ~ 0
D4
Text HLabel 8450 2600 0    50   Input ~ 0
D5
Text HLabel 9900 2550 0    50   Input ~ 0
D5
Text HLabel 9900 2650 0    50   Input ~ 0
D6
$EndSCHEMATC
