#include <Wire.h>
#include "Adafruit_MCP23017.h"
#include <Keyboard.h>

#define FN_KEY 37

#define DEBUG_KEYPRESS false

byte keys[8][8];

uint8_t matrixRowPins[8] = {0, 1, 2, 3, 4, 5, 6, 7};
uint8_t matrixColPins[8] = {A3, A2, A1, A0, 15, 14, 16, 10};

byte keyState[256];
byte keyStatePrev[256];

//                 0         1      2       3       4       5       6       7       8        9    10   11   12   13   14   15   16             17   18   19   20   21   22   23   24              25   26   27   28   29   30   31   32             33            34            35   36             37Fn 38              39   40      41      42       43   44   45              46   47   48   49   50   51 '\'   52            53  54          55   56   57   58   59              60              61               62
byte keyCodes[] = {KEY_ESC, '1',    '2',    '3',    '4',    '5',    '6',    '7',    KEY_TAB, 'q', 'w', 'e', 'r', 't', 'y', 'u', KEY_CAPS_LOCK, 'a', 's', 'd', 'f', 'g', 'h', 'j', KEY_LEFT_SHIFT, 'z', 'x', 'c', 'v', 'b', 'n', 'm', KEY_LEFT_CTRL, KEY_LEFT_GUI, KEY_LEFT_ALT, ' ', KEY_RIGHT_ALT, ' ', KEY_RIGHT_CTRL, ' ', '8',    '9',    '0',     '-', '=', KEY_BACKSPACE , 'i', 'o', 'p', '[', ']', 92,      ';',          39, KEY_RETURN, '?', 'k', 'l', ',', '.',            '/',            KEY_DELETE,      KEY_RIGHT_SHIFT};
byte fnCodes[] =  {'`',     KEY_F1, KEY_F2, KEY_F3, KEY_F4, KEY_F5, KEY_F6, KEY_F7, KEY_TAB, 'q', 'w', 'e', 'r', 't', 'y', 'u', KEY_CAPS_LOCK, 'a', 's', 'd', 'f', 'g', 'h', 'j', KEY_LEFT_SHIFT, 'z', 'x', 'c', 'v', 'b', 'n', 'm', KEY_LEFT_CTRL, KEY_LEFT_GUI, KEY_LEFT_ALT, ' ', KEY_RIGHT_ALT, ' ', KEY_RIGHT_CTRL, ' ', KEY_F8, KEY_F9, KEY_F10, '-', '=', KEY_BACKSPACE , 'i', 'o', 'p', '[', ']', 92,      KEY_UP_ARROW, 39, KEY_RETURN, '?', 'k', 'l', ',', KEY_LEFT_ARROW, KEY_DOWN_ARROW, KEY_RIGHT_ARROW, KEY_RIGHT_SHIFT};


Adafruit_MCP23017 mcp;

long last = 0;

void setup() {
  mcp.begin();      // use default address 0
  initOLED();
  Keyboard.begin();
}



void loop() {
  last = millis();

  // record previous keyboard state
  for (int i = 0; i < 256; i++) {
    keyStatePrev[i] = keyState[i];
  }

  ScanMatrix();
  
  // Announce Key actions 
  for (int i = 0; i < 256; i++) {
    if (i == FN_KEY) continue; //skip Fn key
    if (keyState[i] != keyStatePrev[i]) {
      
      if (keyState[i]) {
        KeyUp(i);
      } else {
        KeyDown(i);
      }
    }
  }
}

void KeyUp(int keyIndex) {
  if (DEBUG_KEYPRESS) {
    Serial.print("Key ");
    Serial.print(keyIndex);
    Serial.print(" ");
    Serial.print(keyCodes[keyIndex]);
    Serial.println(" UP");
  }
  
  //if (fnDown()) {
  Keyboard.release(fnCodes[keyIndex]);
  //} else {
  Keyboard.release(keyCodes[keyIndex]);
  //}


}

void KeyDown(int keyIndex) {
  if (DEBUG_KEYPRESS) {
    Serial.print("Key ");
    Serial.print(keyIndex);
    Serial.print(" ");
    Serial.print(keyCodes[keyIndex]);
    Serial.println(" Down");
  }
  PrintChar(keyCodes[keyIndex]);
  
  if (fnDown()) {
    Keyboard.press(fnCodes[keyIndex]);
  } else {
    Keyboard.press(keyCodes[keyIndex]);
  }

}

bool fnDown() {
  return !keyState[FN_KEY];
}

void ScanMatrix(){
for (int colIndex = 0; colIndex < 8; colIndex++)
  {
    mcp.pinMode(matrixRowPins[colIndex], OUTPUT);
    mcp.digitalWrite(matrixRowPins[colIndex], LOW);

    for (int rowIndex = 0; rowIndex < 8; rowIndex++)
    {
      pinMode(matrixColPins[rowIndex], INPUT_PULLUP);

      keyState[colIndex + rowIndex * 8] = digitalRead(matrixColPins[rowIndex]);

      pinMode(matrixColPins[rowIndex], INPUT);
    }

    // disable the column
    mcp.pinMode(matrixRowPins[colIndex], INPUT);
  }
}
