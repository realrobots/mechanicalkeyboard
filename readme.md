## RealRobots Mechanical Keyboard

#### Introduction

This project is a 62 key mechanical keyboard, planned for use with a future Raspberry Pi 4 based cyberdeck/laptop. 

![v1_layout](pics/rr_kbd_6.jpg)

#### Status

Current status is PCB designed and undergoing final revision before manufacture of first prototype



#### Electronics

The keyswitches are laid out in an 8x8 matrix with diodes on each key to prevent ghosting. The 8 rows and 8 columns are read by an MCP23017 GPIO Expander connected to an Arduino Pro Micro. Also connected to the Pro Micro is an i2c OLED 128x32 pixel display which will be in the top right corner of the keyboard and will display capslock status and battery level. There are 4 analog inputs broken out from the Arduino to the board for connection of battery monitor and possible future peripherals.


#### Layout

![v1_layout](v1_layout.jpg)



#### PCB Render

![v1_layout](v1_pcb_render.jpg)


## Components

The keyswitches are pretty standard ones that I picked for being the cheapest available on Taobao. Of the options available these ones have a nice clicky sound.
Similarly the keycaps are pretty standard ABS caps that I got in a full set for an absolute steal, the plan is to buy them in a bunch of different colours so
students can mix and match colours.

![v1_layout](v1_keyswitches_keycaps.jpg)


![v1_layout](pics/rr_kbd_7.jpg)